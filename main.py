from unidecode import unidecode

#Demande du texte à vérifier
phrase = input("Saisissez votre texte à vérifier : ")

#suppression des accents par Unidecode
phrase = unidecode(phrase)

#suppression des caractères (échappement des caractères)
symb = ["'", ":", ";", ",", "-", "?", "!", " ", "."]

for i in phrase :
    if i in symb :
        phrase=phrase.replace(i,"")

#ecriture dans palindrome de la phrase en inversée tout en la mettant en lettre minuscule
palindrome = phrase[::-1].lower()

#comparaison entre la phrase inversée est égale à la phrase saisie par l'utilisateur
if phrase.lower() == palindrome:
    print(phrase, "est un palindrome")
else:
    print(phrase, "n'est pas un palindrome")

print ("merci, au revoir")

